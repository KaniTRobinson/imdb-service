const express = require('express')
const router = express.Router()

router.use('/imdb', require('./imdb.routes'))

module.exports = router
