const express = require('express')
const imdbController = require('../controllers/imdb.controller')

const router = express.Router()

router.get('/', imdbController.getByIds)
router.get('/:imdbId', imdbController.getById)
router.get('/range/:startDate/:endDate', imdbController.getByDateRange)

module.exports = router
