const axios = require('axios')
const { MOVIE_SOURCE_SERVICE } = process.env

class MovieSourceServiceClient {
  async send(id, type, series = null, episode = null, parentId = null) {
    try {
      console.table({ id, type, series, episode, parentId })
      if (type === 'tv') return null
      if (type === 'movie') return axios.get(`${MOVIE_SOURCE_SERVICE}/two-embed/movie/${id}`)
      return axios.get(`${MOVIE_SOURCE_SERVICE}/two-embed/tv/${parentId}/${series}/${episode}?childId=${id}`)
    } catch (err) {
      console.error(id, err)
      return {}
    }
  }
}

module.exports = new MovieSourceServiceClient()
