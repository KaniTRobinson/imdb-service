const express = require('express')
const router = require('./routes')
const app = express()

app.use(function (req, res, next) {
  req.setTimeout(0)
  next()
})
app.use('/api/v1', router)

module.exports = app
