const axios = require('axios')
const cheerio = require('cheerio')
const movieSourceServiceClient = require('../clients/movie-source-service.client')
const sleep = require('../utils/sleep')

class ImdbService {
  /**
   * Get the api url for date range.
   * @param {string} startDate
   * @param {string} endDate
   * @param {string} url
   * @returns
   */
  getDateRangeUrl(startDate, endDate, url = null) {
    if (url) return `https://www.imdb.com${url}`
    if (startDate === endDate) return `https://www.imdb.com/search/title/?year=${startDate}`
    return `https://www.imdb.com/search/title/?year=${startDate},${endDate}`
  }

  async getHtml(url) {
    const response = await axios.get(url).catch((err) => console.log(err.message))
    return cheerio.load(response.data || '')
  }

  /**
   * Scrape by a date range
   * @param {string} startDate
   * @param {string} endDate
   * @param {string} url
   * @returns Array
   */
  async scrapeByDateRange(startDate, endDate, url = null) {
    const apiUrl = this.getDateRangeUrl(startDate, endDate, url)
    const $ = await this.getHtml(apiUrl)
    const nextUrl = $('a.next-page').attr('href')
    const imdbIds = $('.lister-item-header > a:last-of-type')
      .toArray()
      .map((item) => $(item).attr('href').replace('/title/', '').replace('/?ref_=adv_li_tt', ''))

    for (const imdbId of imdbIds) {
      await this.scrapeById(imdbId)
      sleep(2000)
    }

    if (nextUrl) {
      console.log(nextUrl)
      const paginatedImdbIds = await this.scrapeByDateRange(startDate, endDate, nextUrl).catch((err) =>
        console.log(err.message)
      )

      sleep(2000)
      return [...imdbIds, ...paginatedImdbIds]
    }

    console.log('Finito')

    return imdbIds
  }

  /**
   * Scrape by imdb ids
   * @param {array} data
   * @param {browser|null} parentBrowser
   * @returns Array
   */
  async scrapeByIds(data) {
    for (const imdb of data) {
      const { id } = imdb
      await this.scrapeById(id)
      await sleep(2000)
    }

    return data
  }

  /**
   * Scrape individually by id.
   * @param {string} id
   * @returns Object|Array
   */
  async scrapeById(id) {
    const $ = await this.getHtml(`https://www.imdb.com/title/${id}`)
    const parent = $('a[data-testid="hero-subnav-bar-all-episodes-button"]').attr('href')
    const parentId = parent && parent.replace('/episodes/?ref_=tt_ep_epl', '').replace('/title/', '')
    const [series, episode] = $('[data-testid="hero-subnav-bar-season-episode-numbers-section"] > li')
      .toArray()
      .map((i) => parseInt($(i).text().replace('S', '').replace('E', '')))

    await movieSourceServiceClient.send(id, parentId ? 'episode' : 'movie', series, episode, parentId)

    return { id, type: parentId ? 'episode' : 'movie', series, episode, parentId }
  }
}

module.exports = new ImdbService()
