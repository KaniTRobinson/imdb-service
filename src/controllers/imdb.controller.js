const imdbService = require('../services/imdb.service')
const getImdbIdsFromRequest = require('../utils/get-imdb-ids-from-request')
class ImdbController {
  async getByDateRange(req, res) {
    try {
      req.setTimeout(0)
      const { startDate, endDate } = req.params
      return res.send({ data: await imdbService.scrapeByDateRange(startDate, endDate) })
    } catch (err) {
      return res.send({ error: err.message })
    }
  }

  async getByIds(req, res) {
    try {
      req.setTimeout(0)
      const imdbIds = getImdbIdsFromRequest(req)
      return res.send({ data: await imdbService.scrapeByIds(imdbIds) })
    } catch (err) {
      return res.send({ error: err.message })
    }
  }

  async getById(req, res) {
    try {
      req.setTimeout(0)
      const { imdbId } = req.params
      return res.send({ data: await imdbService.scrapeById(imdbId) })
    } catch (err) {
      return res.send({ error: err.message })
    }
  }
}

module.exports = new ImdbController()
